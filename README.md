# Challenge 5 HTTP Server by Team 3

Berikut ini merupakan gambar dari diagram tabel Cars sebagai berikut:

![alt text](public/images/diagram.jpeg)

## Install dependency

```bash
# Pengguna NPM

npm init
npm install

```

```bash
# Pengguna Yarn

yarn init
yarn install

```

## Install Sequelize

```
npm install sequelize
npm install --save-dev sequelize-cli
```

## Configuration Env

Before continuing further we will need to tell the sequelize how to connect to the database. To do that let's open default config file config/config.json. The keys of the objects (e.g "development") are used on model/index.js for matching process.env.NODE_ENV (When undefined, "development" is a default value). So, we have to create a file .env which contains:

```
DB_USERNAME=
DB_PASSWORD=
DB_NAME=
PORT=


Note: If the database that we want doesn't exist yet, we can just call db:create command. With proper access it will create that database for us.
```

## Configuration Sequelize Migrations

Once you have properly configured sequelize config file we are ready to create our first migration. It's as simple as executing a simple command.

```
npx sequelize init
npx sequelize model:generate --name Cars --attributes name:string,rent:string,size:string
```

## Running The Migrations

```
npx sequelize db:migrate --env development
npx sequelize db:migrate
```

## Create Seed

```
npx sequelize seed:generate --name add-cars
```

## Running Seed

```
npx sequelize db:seed:all
```

## The .sequelizerc File

```
This is a special configuration file. It lets you specify the following options that you would usually pass as arguments to sequelize

const dotenv = require('dotenv');
const path = require('path');

// To read dotenv during the migration
dotenv.config()

module.exports = {
  'config': path.resolve('config', 'database.js'),
}
```

## REST API

```
The REST API to the example app is described below.
```

```bash
# GET Car

router.get('/api/cars', Cars.findCars)
localhost:3000/api/cars
router.get('/api/cars/:id', Cars.findCarsById)
localhost:3000/api/cars/id
```

```bash
# POST Car

router.post('/api/cars', uploader.single('image'), Cars.createCars)
localhost:3000/api/cars
```

```bash
# PUT Car

router.put('/api/cars/:id', Cars.updateCars)
localhost:3000/api/cars/id
```

```bash
# GET Car

router.delete('/api/cars/:id', Cars.deleteCars)
localhost:3000/api/cars/id
```
