const { Cars } = require('../models')
const imagekit = require('../lib/imageKit')

const createCars = async (req, res) => {
    const { name, rent, size, warehouse } = req.body
    // req.body.name, req.body.price, req.body.quantity
    try {
        // untuk dapat extension file nya
        const split = req.file.originalname.split('.')
        const ext = split[split.length - 1]

        // upload file ke imagekit
        const img = await imagekit.upload({
            file: req.file.buffer, //required
            fileName: `${req.file.originalname}.${ext}`, //required
        })

        const newCars = await Cars.create({
            name,
            rent,
            size,
            image: img.url,
            warehouse
        })

        res.status(201).json({
            status: 'Success',
            data: {
                newCars
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'Failed',
            errors: [err.message]
        })
    }
}

const findCars = async (req, res) => {
    try {
        const findCar = await Cars.findAll()
        res.status(200).json({
            status: 'Success',
            data: {
                findCar
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'Failed',
            errors: [err.message]
        })
    }
}

const findCarsById = async (req, res) => {
    try {
        const findCars = await Cars.findOne({
            where: {
                id: req.params.id
            }
        })
        res.status(200).json({
            status: 'Success',
            data: {
                findCars
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'Failed',
            errors: [err.message]
        })
    }
}

const updateCars = async (req, res) => {
    try {
        const { name, rent, size } = req.body
        const id = req.params.id
        const car = await Cars.update({
            name,
            rent,
            size
            
        }, {
            where: {
                id
            }
        })
        res.status(200).json({
            status: 'Success',
            data: {
                id, name, rent, size
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'Failed',
            errors: [err.message]
        })
    }
}

const deleteCars = async (req, res) => {
    try {
        const id = req.params.id
        await Cars.destroy({
            where: {
                id
            }
        })
        res.status(200).json({
            status: 'Success',
            message: `Cars with id ${id} have been deleted`
        })
    } catch (err) {
        res.status(400).json({
            status: 'Failed',
            errors: [err.message]
        })
    }
}

module.exports = {
    createCars,
    findCars,
    findCarsById,
    updateCars,
    deleteCars
}