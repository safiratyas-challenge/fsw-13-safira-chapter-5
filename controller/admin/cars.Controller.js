const { Cars } = require("../../models");
const { Op } = require("sequelize");
const imagekit = require("../../lib/imageKit");

const homepage = async (req, res) => {
  res.render("index", {
    title: "Car Management Dashboard",
  });
};

const dataPage = async (req, res) => {
  const items = await Cars.findAll();
  res.render("index", {
    items,
    message: req.flash("message"),
  });
};

const createPage = async (req, res) => {
  try {
    res.render("Create", {
      title: "Create Cars Data",
    });
  } catch (error) {
    res.status(404).json({
      message: error.message,
    });
  }
};

const createCar = async (req, res) => {
  try {
    var { name, rent, size } = req.body;

    // req.body.name, req.body.price, req.body.quantity
    // untuk dapat extension file nya
    const split = req.file.originalname.split(".");
    const ext = split[split.length - 1];

    // upload file ke imagekit
    const img = await imagekit.upload({
      file: req.file.buffer, //required
      fileName: `${req.file.originalname}.${ext}`, //required
    });
    console.log(img.url);

    await Cars.create({
      name,
      rent,
      size,
      image: img.url,
    });
    req.flash("message", "Data saved successfully");
    res.redirect("/");
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};

const editPage = async (req, res) => {
  const items = await Cars.findByPk(req.params.id);
  res.render("edit", {
    items,
    title: "Edit Data",
  });
};

const editCars = async (req, res) => {
  try {
    var { name, rent, size } = req.body;
    name = name.toLowerCase();
    const id = req.params.id;

    const split = req.file.originalname.split(".");
    const ext = split[split.length - 1];

    const img = await imagekit.upload({
      file: req.file.buffer, //required
      fileName: `${req.file.originalname}.${ext}`, //required
    });
    console.log(img.url);

    await Cars.update(
      {
        name,
        rent,
        size,
        image: img.url,
      },
      {
        where: {
          id,
        },
      }
    );
    req.flash("message", "Data updated successfully");
    res.redirect("/");
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};

const deleteCars = async (req, res) => {
  const id = req.params.id;
  await Cars.destroy({
    where: {
      id,
    },
  });
  req.flash("message", "Data deleted successfully");
  res.redirect("/");
};

const filterPage = async (req, res) => {
  try {
    let ukuran = req.query.size;
    if (ukuran == "All") {
      const items = await Cars.findAll({
        where: {
          [Op.or]: [{ size: "Small" }, { size: "Medium" }, { size: "Large" }],
        },
      });
      res.render("filter", {
        items,
      });
    } else {
      const items = await Cars.findAll({
        where: {
          size: ukuran,
        },
      });
      res.render("filter", {
        items,
      });
    }
  } catch (error) {
    res.status(404).json({
      message: error.message,
    });
  }
};

const searchCars = async (req, res) => {
  const query = req.query.query;
  const items = await Cars.findAll({
    where: {
      name: {
        [Op.like]: `%${query}%`,
      },
    },
  });
  res.render("search", {
    title: "search",
    items,
  });
};

module.exports = {
  homepage,
  dataPage,
  createPage,
  createCar,
  editPage,
  editCars,
  deleteCars,
  filterPage,
  searchCars,
};
