const router = require("express").Router();
const Cars = require("../controller/carsController");
const Upload = require("../controller/uploadController");
const Admin = require("../controller/admin/cars.Controller");

// Middleware
const uploader = require("../middlewares/uploader");

// API Server
router.post("/api/cars", Cars.createCars);
router.get("/api/cars", Cars.findCars);
router.get("/api/cars/:id", Cars.findCarsById);
router.put("/api/cars/:id", Cars.updateCars);
router.delete("/api/cars/:id", Cars.deleteCars);

// Admin Client Side
router.get("/", Admin.dataPage);
router.get("/admin/create", Admin.createPage);
router.post("/admin/car", uploader.single("image"), Admin.createCar);
router.get("/admin/car/:id", Admin.editPage);
router.post("/admin/update/:id", uploader.single("image"), Admin.editCars);
router.post("/admin/delete/:id", Admin.deleteCars);
router.get("/admin/filter", Admin.filterPage);

// Search
router.get("/admin/search", Admin.searchCars);

// test upload image
router.post("/api/upload", uploader.single("image"), Upload.uploadImage);

module.exports = router;
